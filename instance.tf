#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
resource "aws_instance" "this" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t3.micro"
  associate_public_ip_address = true
  key_name                    = var.ssh_key
  security_groups = [
    "${aws_security_group.instance.name}",
    "${aws_security_group.admin.name}"
  ]
  user_data = data.template_file.instance.rendered
  tags      = var.default_tags
}
