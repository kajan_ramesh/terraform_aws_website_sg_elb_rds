variable "region" {
  type        = string
  default     = "eu-west-1"
  description = "Define default region"
}

variable "prefix" {
  type        = string
  default     = "demo"
  description = "Define a prefix for all AWS ressources (Only numbers and letters)"
}

variable "ssh_key" {
  type        = string
  default     = "demo-prof"
  description = "You can define the AWS key to use"
}

variable "my_public_key" {
  type        = string
  default     = ""
  description = "You can add a public key to the instance for user root"
}

variable "default_tags" {
  type = map(string)
  default = {
    "Name" = "demo"
  }
}

variable "subnetes_region" {
  type        = list(string)
  description = "List of ID for all network the selected region"
}

variable "rds_engine_type" {
  type        = string
  default     = "mysql"
  description = "The engine for RDS"
}

variable "rds_engine_version" {
  type        = string
  default     = "8.0.26"
  description = "The engine version for RDS"
}

variable "rds_user" {
  type        = string
  default     = "main"
  description = "The base user RDS"
}

variable "rds_instance_size" {
  type        = string
  default     = "db.t2.micro"
  description = "The instance size"
}

variable "instance_size" {
  type        = string
  default     = "t2.micro"
  description = "Instance size"
}
