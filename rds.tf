#Dans une entreprise, on serait passé par un gestionnaire de secret comme Vault ou encore le service SecretManager
resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "@"
}

resource "aws_db_instance" "this" {
  allocated_storage   = 20
  engine              = var.rds_engine_type
  engine_version      = var.rds_engine_version
  instance_class      = var.rds_instance_size
  name                = var.prefix
  username            = var.rds_user
  password            = random_password.password.result
  skip_final_snapshot = true
  vpc_security_group_ids = [
    "${aws_security_group.rds.id}"
  ]
  #Le RDS ne peut pas être chiffré avec les instances db.t2.micro
  #storage_encrypted = true
  #kms_key_id = aws_kms_key.main.arn
  tags = var.default_tags
}
