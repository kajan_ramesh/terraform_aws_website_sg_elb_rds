terraform {
  required_providers {
    aws = {
      version = ">= 3.63.0"
      source  = "hashicorp/aws"
    }
    random = {
      version = ">= 3.1.0"
      source  = "hashicorp/random"
    }
    http = {
      version = ">= 2.1.0"
      source  = "hashicorp/http"
    }
    template = {
      version = ">= 2.2.0"
      source  = "hashicorp/template"
    }
  }
}

provider "aws" {
  region = var.region
}

#Exemple de backend
#terraform {
#  backend "s3" {
#    bucket         = "prof-states"
#    key            = "state/terraform-demo.tfstate"
#    region         = "eu-west-1"
#    dynamodb_table = "prof-states"
#  }
