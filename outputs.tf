output "instance_ip" {
  value = aws_instance.this.public_ip
}

output "lb_endpoint" {
  value = aws_lb.this.dns_name
}
