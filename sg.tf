resource "aws_security_group" "lb" {
  name        = "${var.prefix}-lb"
  description = "Security group for LB instance"
  vpc_id      = data.aws_vpc.default.id
  tags        = var.default_tags

  ingress {
    description      = "Allow 80"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "instance" {
  name        = "${var.prefix}-instance"
  description = "Security group for EC2 instance"
  vpc_id      = data.aws_vpc.default.id
  tags        = var.default_tags

  ingress {
    description     = "Allow 80"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${aws_security_group.lb.id}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
# On fait un security group à part pour les accès admin, il aurait été possible de le faire dans le sg instance, ici c'était juste pour vous donner un autre exemple.
resource "aws_security_group" "admin" {
  name        = "${var.prefix}-admin"
  description = "Add security for administration"
  vpc_id      = data.aws_vpc.default.id
  tags        = var.default_tags

  ingress {
    description = "Allow 80"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "rds" {
  name        = "${var.prefix}-rds"
  description = "Security group for RDS instance"
  vpc_id      = data.aws_vpc.default.id
  tags        = var.default_tags

  ingress {
    description     = "Allow 3306"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.instance.id}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
