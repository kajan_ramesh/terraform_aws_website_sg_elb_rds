# Correction TP1 AWS

Correction du TP1 AWS en Terraform.

## Sujet :

* Il faut créer une instance EC2 (Debian ou Ubuntu) qui expose le port 80 derrière un loadbalancer (NLB ou ALB)
* Installer sur l'instance un serveur Web (Nginx ou Apache2 fonctionne)
* Installer un RDS (MySQL) uniquement accessible par l'instance EC2 (à sécuriser avec les Security Group)
* Se connecter avec la CLI de l'EC2 à l'instance MySQL (le client s'appel mysql-client)
				
Bonus : vous pouvez installer Wordpress sur l'instance avec le RDS en SGBD

## Notes :

* Le code utilise directement le VPC par défaut. En entreprise, il ne faut pas utiliser les VPCs créé par défaut et préférer créer votre VPC avec une topologie adaptée
* Pour executer le code vous pouvez adapter le fichier de variable `./demo.tfvars`
* L'execution peut se faire à l'aide du fichier `./Makefile` que vous pouvez lancer avec la commande : `make tf_`
* Je vous recommande d'installer est d'utiliser [tfenv](https://github.com/tfutils/tfenv) pour gérer votre installation de Terraform. Il se basera automatiquement sur le fichier `.terraform-version`
* Une configuration pour que les states soit dans un bucket plutôt qu'en local avec une instance DynamoDB pour gérer les locks est dispo (mais commenté dans : `./provider.tf`)
